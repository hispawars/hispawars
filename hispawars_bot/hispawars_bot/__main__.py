from . import Ants, SampleBot


if __name__ == '__main__':
    try:
        Ants.run(SampleBot())
    except KeyboardInterrupt:
        print('ctrl-c, leaving ...')
